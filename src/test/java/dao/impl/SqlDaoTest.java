package dao.impl;

import model.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class SqlDaoTest {

    SqlDao mySqlDao = new MySqlDao();

    @Test
    public void getDBConnection() {
        //Assert.assertNull(mySqlDao.getDBConnection());
    }

    @Test
    public void create() {
        int sizeAcyal = mySqlDao.read().size();
        mySqlDao.create(new Person(0,"","",0,""));
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeAcyal+1,sizeBefore);
        mySqlDao.deleteAll();
    }

    @Test
    public void read() {
        mySqlDao.deleteAll();
        int sizeAcyal = mySqlDao.read().size();
        mySqlDao.create(new Person(0,"","",0,""));
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeAcyal,sizeBefore-1);
        mySqlDao.deleteAll();
    }

    @Test
    public void delete() {
        mySqlDao.create(new Person(500,"","",0,""));
        int sizeAcyal = mySqlDao.read().size();
        mySqlDao.delete(500);
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeAcyal,sizeBefore);
    }

    @Test
    public void update() {
        mySqlDao.create(new Person(500,"","",0,""));
        int sizeAcyal = mySqlDao.read().size();
        mySqlDao.update(new Person(500,"Artemiy","Brook",20,"Grey"));
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeAcyal,sizeBefore);
    }

    @Test
    public void deleteAll() {
        //mySqlDao.deleteAll();
        mySqlDao.create(new Person(500,"","",0,""));
        mySqlDao.deleteAll();
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(0,sizeBefore);
    }

    @Test
    public void deleteAllbyName() {
        mySqlDao.deleteAll();
        mySqlDao.create(new Person(0,"Abc","",0,""));
        int sizeAfter = mySqlDao.read().size();
        mySqlDao.deleteAllbyName("Abc");
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void deleteAllbyLastName() {
        mySqlDao.deleteAll();
        mySqlDao.create(new Person(0,"","Abc",0,""));
        int sizeAfter = mySqlDao.read().size();
        mySqlDao.deleteAllbyLastName("Abc");
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void deleteAllbyAge() {
        mySqlDao.deleteAll();
        mySqlDao.create(new Person(0,"","Abc",999,""));
        int sizeAfter = mySqlDao.read().size();
        mySqlDao.deleteAllbyAge(999);
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void deleteAllbyCity() {
//        mySqlDao.deleteAll();
        mySqlDao.create(new Person(0,"","",0,"Abc"));
        int sizeAfter = mySqlDao.read().size();
        mySqlDao.deleteAllbyCity("Abc");
        int sizeBefore = mySqlDao.read().size();
        Assert.assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void getById() {
        Person after = new Person(40,"","",0,"");
        mySqlDao.create(after);
        Person before = mySqlDao.getById(40);
        Assert.assertEquals(after.getId(),before.getId());
    }

    @Test
    public void getByName() {
        Person after = new Person(0,"abc","",0,"");
        mySqlDao.create(after);
        List<Person> people = mySqlDao.getByName("abc");
        Assert.assertEquals(after.getName(),people.get(0).getName());
    }

    @Test
    public void getByLastName() {
        Person after = new Person(0,"","abc",0,"");
        mySqlDao.create(after);
        List<Person> people = mySqlDao.getByLastName("abc");
        Assert.assertEquals(after.getLname(),people.get(0).getLname());
    }

    @Test
    public void getByAge() {
        Person after = new Person(0,"","",20,"");
        mySqlDao.create(after);
        List<Person> people = mySqlDao.getByAge(20);
        Assert.assertEquals(after.getAge(),people.get(0).getAge());
    }

    @Test
    public void getByCity() {
        Person after = new Person(0,"","",0,"abc");
        mySqlDao.create(after);
        List<Person> people = mySqlDao.getByCity("abc");
        Assert.assertEquals(after.getCity(),people.get(0).getCity());
    }
}
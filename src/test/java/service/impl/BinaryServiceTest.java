package service.impl;

import model.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BinaryServiceTest {

    BinaryService binaryService = new BinaryService();
    
    @Test
    public void create() {
        int sizeAcyal = binaryService.read().size();
        binaryService.create(new Person(0,"","",0,""));
        int sizeBefore = binaryService.read().size();
        Assert.assertEquals(sizeAcyal+1,sizeBefore);
        binaryService.deleteAll();
    }

    @Test
    public void read() {
        binaryService.deleteAll();
        int sizeAcyal = binaryService.read().size();
        binaryService.create(new Person(0,"","",0,""));
        int sizeBefore = binaryService.read().size();
        Assert.assertEquals(sizeAcyal,sizeBefore-1);
        binaryService.deleteAll();
    }

    @Test
    public void delete() {
        binaryService.create(new Person(500,"","",0,""));
        int sizeAcyal = binaryService.read().size();
        binaryService.delete(500);
        int sizeBefore = binaryService.read().size();
        assertEquals(0,sizeBefore);
    }

    @Test
    public void update() {
        binaryService.create(new Person(500,"","",0,""));
        int sizeAcyal = binaryService.read().size();
        binaryService.update(new Person(500,"Artemiy","Brook",20,"Grey"));
        assertEquals(sizeAcyal,1);
    }

    @Test
    public void deleteAll() {
        binaryService.deleteAll();
        binaryService.create(new Person(500,"","",0,""));
        binaryService.deleteAll();
        int sizeBefore = binaryService.read().size();
        assertEquals(0,sizeBefore);
    }

    @Test
    public void deleteAllbyName() {
        binaryService.deleteAll();
        binaryService.create(new Person(0,"Abc","",0,""));
        int sizeAfter = binaryService.read().size();
        binaryService.deleteAllbyName("Abc");
        int sizeBefore = binaryService.read().size();
        assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void deleteAllbyLastName() {
        binaryService.deleteAll();
        binaryService.create(new Person(0,"","Abc",0,""));
        int sizeAfter = binaryService.read().size();
        binaryService.deleteAllbyLastName("Abc");
        int sizeBefore = binaryService.read().size();
        assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void deleteAllbyAge() {
        binaryService.deleteAll();
        binaryService.create(new Person(0,"","Abc",999,""));
        int sizeAfter = binaryService.read().size();
        binaryService.deleteAllbyAge(999);
        int sizeBefore = binaryService.read().size();
        assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void deleteAllbyCity() {
        binaryService.deleteAll();
        binaryService.create(new Person(0,"","",0,"Abc"));
        int sizeAfter = binaryService.read().size();
        binaryService.deleteAllbyCity("Abc");
        int sizeBefore = binaryService.read().size();
        assertEquals(sizeBefore+1,sizeAfter);
    }

    @Test
    public void getById() {
        Person after = new Person(40,"","",0,"");
        binaryService.create(after);
        Person before = binaryService.getById(40);
        assertEquals(after.getId(),before.getId());
    }

    @Test
    public void getByName() {
        Person after = new Person(0,"abc","",0,"");
        binaryService.create(after);
        List<Person> people = binaryService.getByName("abc");
        assertEquals(after.getName(),people.get(0).getName());
    }

    @Test
    public void getByLastName() {
        Person after = new Person(0,"","abc",0,"");
        binaryService.create(after);
        List<Person> people = binaryService.getByLastName("abc");
        assertEquals(after.getLname(),people.get(0).getLname());
    }

    @Test
    public void getByAge() {
        Person after = new Person(0,"","",20,"");
        binaryService.create(after);
        List<Person> people = binaryService.getByAge(20);
        assertEquals(after.getAge(),people.get(0).getAge());
    }

    @Test
    public void getByCity() {
        Person after = new Person(0,"","",0,"abc");
        binaryService.create(after);
        List<Person> people = binaryService.getByCity("abc");
        assertEquals(after.getCity(),people.get(0).getCity());
    }
}
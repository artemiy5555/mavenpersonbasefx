import dao.impl.H2Dao;
import dao.impl.HibernateDao;
import dao.impl.MongoDAO;
import model.Person;
import service.impl.MySqlService;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {

//    private static final String USER = "root";
//    private static final String PASS = "root";
//
//    private static final String URL_H2 =
//            "jdbc:h2:D:/H2db/new_schema";

    private static final String USER = "root";
    private static final String PASS = "root";

    private static final String URL_MYSQL =
            "jdbc:mysql://34.67.123.43:3306/new_schema?useSSL=false";


    public static void main(String[] args) {

        MongoDAO mongoDAO = new MongoDAO();
        mongoDAO.create(new Person(21313,"","",21,""));

//        MySqlService servicePostSql =new MySqlService("root","root","jdbc:postgresql://104.198.210.244:5432/postgres");
//        servicePostSql.deleteAll();
//        servicePostSql.create(new Person("Artemiy","Brrok",20,"Kiev"));
//        servicePostSql.read();
//        System.out.println("\n\n\n");
//        MySqlService serviceMySql =new MySqlService("root","root","jdbc:mysql://34.67.123.43:3306/new_schema?useSSL=false");
//        serviceMySql.create(new Person("Artemiy","Brrok",20,"Kiev"));
//        serviceMySql.read();
//        System.out.println("\n\n\n");

//        HibernateService userService = new HibernateService();
//        //userService.findUser(1);
//        userService.create(new Person("Dodic","San",20,"Kiev"));
//        //userService.deleteAll();
//        ArrayList<Person> allPerson = userService.read();
//        for(Person person : allPerson){
//            System.out.println(person.toString());
//        }

//        MySqlDao mySqlDao = new MySqlDao();
//
//        mySqlDao.read();
//
//        System.out.println();
//        System.out.println();
//        System.out.println();
//
//        PostgresDao postgresUtil = new PostgresDao();
//
//        postgresUtil.read();


//        HibernateDao hibernateDao = new HibernateDao();
//        ArrayList<Person> arrayList=hibernateDao.read();
//
//        for(Person person:arrayList)
//        System.out.println(person);

//        ArrayList<Person> arrayList = hibernateDao.getByName("Dodic");
//        for(Person person:arrayList)
//            System.out.println(person);

//        MySqlService mySqlService = new MySqlService();
//        mySqlService.read();

//        Connection conn = null;
//        Statement stmt = null;
//        try{
//            //STEP 2: Register JDBC driver
//            Class.forName("com.mysql.jdbc.Driver");
//
//            //STEP 3: Open a connection
//            System.out.println("Connecting to a selected database...");
//            conn = DriverManager.getConnection(URL_MYSQL, USER, PASS);
//            System.out.println("Connected database successfully...");
//
//            //STEP 4: Execute a query
//            System.out.println("Creating table in given database...");
//            stmt = conn.createStatement();
//
//            String schema ="CREATE SCHEMA new";
//            stmt.executeUpdate(schema);
//            String sql = "CREATE TABLE new.persons(id bigint auto_increment PRIMARY KEY, NAME VARCHAR(255), LNAME VARCHAR(255), AGE INT, CITY VARCHAR(255));";
//
//            stmt.executeUpdate(sql);
//            System.out.println("Created table in given database...");
//        }catch(SQLException se){
//            //Handle errors for JDBC
//            se.printStackTrace();
//        }catch(Exception e){
//            //Handle errors for Class.forName
//            e.printStackTrace();
//        }

//        MySqlService mySqlService = new MySqlService();
//        mySqlService.create(new Person(36,"Artemiy","Baranets",20,"Kiev"));
//        System.out.println(mySqlService.getById(36));
//        //h2Dao.create(new Person("Artemiy","Baranets",12,"Kiev"));




    }

}

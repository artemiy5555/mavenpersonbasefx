import dao.impl.H2Dao;
import dao.impl.MySqlDao;
import dao.impl.PostgresDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Person;
import service.impl.*;

import java.util.List;

public class Controller {

    //*****TextField*****//
    @FXML
    private TextField firsName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField age;
    @FXML
    private TextField city;
    @FXML
    private TextField id;

    //*****ComboBox*****//
    //private ObservableList<String> langs = FXCollections.observableArrayList("MySql", "MySQL_Hibernate", "PostgreSQL");
    @FXML
    private ComboBox<String> langsComboBox = new ComboBox<>();

    //*****TableView*****//
    @FXML
    private TableView<Person> tablePerson;

    @FXML
    private TableColumn<Person, Integer> idCol;
    @FXML
    private TableColumn<Person, String> nameCol;
    @FXML
    private TableColumn<Person, String> lastNameCol;
    @FXML
    private TableColumn<Person, Integer> ageCol;
    @FXML
    private TableColumn<Person, String> cityCol;

    private ObservableList<Person> personData;
    private List people;
    private AllGRUD allGRUD = new AllGRUD();

    public void initialize() {
        idCol.setCellValueFactory(new PropertyValueFactory<Person, Integer>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("name"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("lname"));
        ageCol.setCellValueFactory(new PropertyValueFactory<Person, Integer>("age"));
        cityCol.setCellValueFactory(new PropertyValueFactory<Person, String>("city"));
        new PostgresDao().postrgesCreateTable();
        new H2Dao().h2CreateTable();
        new MySqlDao().mySqlCreateTable();
        new BinaryService().binaryService();
        new XmlService().xmlService();
        new YamlService().yamlService();
        new CsvService().csvService();
        new JsonService().jsonService();
    }

    //*****GRUD*****//
    @FXML
    private void readBase() {
        personData = null;
        people = allGRUD.getAll(langsComboBox.getValue());
        listToTable(people);
    }

    @FXML
    private void updatePerson() {
//        personData.get();
        //написть апдейт
        List<Person> per =allGRUD.getById(Integer.parseInt(id.getText()),langsComboBox.getValue());
        final Person oldPerson = per.get(0);


        final Stage newWindow = new Stage();
        Button button = new Button("Update");

        final TextField NAME = new TextField();
        NAME.setText(oldPerson.getName());
        final TextField LNAME = new TextField();
        LNAME.setText(oldPerson.getLname());
        final TextField ARE = new TextField();
        ARE.setText(String.valueOf(oldPerson.getAge()));
        final TextField CITY = new TextField();
        CITY.setText(String.valueOf(oldPerson.getCity()));
        Label secondLabel = new Label("Update?");
        FlowPane root = new FlowPane();
        root.setPadding(new Insets(10));
        root.setHgap(10);
        root.setVgap(10);

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String name = NAME.getText();
                String surename = LNAME.getText();
                int are = Integer.parseInt(ARE.getText());
                String city = CITY.getText();
                allGRUD.update(new Person(oldPerson.getId(),name,surename,are,city),langsComboBox.getValue());
                newWindow.close();
                readBase();
            }
        });

        root.getChildren().addAll(secondLabel,NAME,LNAME,ARE,CITY, button);
        Scene secondScene = new Scene(root, 230, 250);
        newWindow.setScene(secondScene);
        newWindow.initModality(Modality.WINDOW_MODAL);
        newWindow.show();

    }

    @FXML
    private void deletePerson() {
        personData = null;
        allGRUD.deleteById(Integer.parseInt(id.getText()), langsComboBox.getValue());
        readBase();
    }

    @FXML
    private void createPerson() {
        int age;
        try {
            age = Integer.parseInt(this.age.getText());
        } catch (NumberFormatException e) {

            Stage newWindow = new Stage();
            Label erorr = new Label();
            erorr.setText("Enter your age digitally!");
            FlowPane root = new FlowPane();
            root.setPadding(new Insets(10));
            root.setHgap(10);
            root.setVgap(10);
            root.getChildren().add(erorr);
            Scene secondScene = new Scene(root, 230, 100);
            newWindow.setScene(secondScene);
            newWindow.initModality(Modality.WINDOW_MODAL);
            newWindow.show();

            e.printStackTrace();
            return;
        }
        allGRUD.insert(new Person(53,
                        firsName.getText(),
                        lastName.getText(),
                        age,
                        city.getText()),
                langsComboBox.getValue());
        readBase();
    }

    //*****Seach*****//
    @FXML
    private void getById() {
        personData = null;
        try {
            people = allGRUD.getById(Integer.parseInt(id.getText()), langsComboBox.getValue());
        }catch (Exception e){
            Stage newWindow = new Stage();
            Label erorr = new Label();
            erorr.setText("Enter your id digitally!");
            FlowPane root = new FlowPane();
            root.setPadding(new Insets(10));
            root.setHgap(10);
            root.setVgap(10);
            root.getChildren().add(erorr);
            Scene secondScene = new Scene(root, 230, 100);
            newWindow.setScene(secondScene);
            newWindow.initModality(Modality.WINDOW_MODAL);
            newWindow.show();

            return;
        }
        listToTable(people);
    }

    @FXML
    private void getAllByName() {
        personData = null;
        people = allGRUD.getByName(firsName.getText(),langsComboBox.getValue());
        listToTable(people);
    }

    @FXML
    private void getAllByLastname() {
        personData = null;
        people = allGRUD.getByLastName(lastName.getText(),langsComboBox.getValue());
        listToTable(people);
    }

    @FXML
    private void getAllByAge() {
        personData = null;
        //дописать проверку приведения!!
        try {
            people = allGRUD.getByAge(Integer.parseInt(age.getText()), langsComboBox.getValue());
        }catch (Exception e){
            Stage newWindow = new Stage();
            Label erorr = new Label();
            erorr.setText("Enter your age digitally!");
            FlowPane root = new FlowPane();
            root.setPadding(new Insets(10));
            root.setHgap(10);
            root.setVgap(10);
            root.getChildren().add(erorr);
            Scene secondScene = new Scene(root, 230, 100);
            newWindow.setScene(secondScene);
            newWindow.initModality(Modality.WINDOW_MODAL);
            newWindow.show();
            return;

        }
        listToTable(people);
    }

    @FXML
    private void getAllByCity() {
        personData = null;
        people = allGRUD.getByCity(city.getText(),langsComboBox.getValue());
        listToTable(people);
    }

    //*****Delete*****//
    @FXML
    private void deleteAll() {
        allGRUD.deleteAll(langsComboBox.getValue());
        readBase();
    }

    @FXML
    private void deleteAllByName() {
        allGRUD.deleteAllByName(firsName.getText(),langsComboBox.getValue());
        readBase();
    }

    @FXML
    private void deleteAllByLastName() {
        allGRUD.deleteAllbyLastName(lastName.getText(),langsComboBox.getValue());
        readBase();
    }

    @FXML
    private void deleteAllByAge() {
        //добавить проверку приведения
        try {
            allGRUD.deleteAllbyAge(Integer.parseInt(age.getText()), langsComboBox.getValue());
        }catch (Exception e){

            Stage newWindow = new Stage();
            Label erorr = new Label();
            erorr.setText("Enter your age digitally!");
            FlowPane root = new FlowPane();
            root.setPadding(new Insets(10));
            root.setHgap(10);
            root.setVgap(10);
            root.getChildren().add(erorr);
            Scene secondScene = new Scene(root, 230, 100);
            newWindow.setScene(secondScene);
            newWindow.initModality(Modality.WINDOW_MODAL);
            newWindow.show();
            return;


        }
        readBase();
    }

    @FXML
    private void deleteAllByCity() {
        allGRUD.deleteAllbyCity(city.getText(),langsComboBox.getValue());
        readBase();
    }

    private void listToTable(List<Person> people) {
        personData = FXCollections.observableArrayList();
        personData.addAll(people);
        tablePerson.setItems(personData);
    }

}

package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Neo4jUtil {

    private static final String USER = "neo4j";
    private static final String PASS = "QgmLjQfRkA6CE92c";

    private static final String URL_ORIENTDB =
            "https://34.73.59.162:7473/\n";

    public static Connection getDBConnection() {
        try {
            Connection dbConnection =
                    DriverManager.getConnection(URL_ORIENTDB, USER,PASS );
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}

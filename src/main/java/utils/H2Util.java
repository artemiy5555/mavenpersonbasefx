package utils;

import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class H2Util {

    private static final String USER = "root";
    private static final String PASS = "root";

    private static final String URL_H2 =
            "jdbc:h2:"+new File("H2db/new_schema").getAbsolutePath();

    public static Connection getDBConnection() {

        try {
            Connection dbConnection =
                    DriverManager.getConnection(URL_H2, USER,PASS );
            return dbConnection;
        } catch (SQLException e) {
            //System.out.println(e.getMessage());
        }
        return null;
    }

}

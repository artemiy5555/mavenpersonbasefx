package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresUtil {

    private static final String USER = "root";
    private static final String PASS = "root";

    private static final String URL_POSTGRESSQL =
            "jdbc:postgresql://104.198.210.244:5432/postgres";

    public static Connection getDBConnection() {
        try {
            Connection dbConnection =
                    DriverManager.getConnection(URL_POSTGRESSQL, USER,PASS );
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }


}

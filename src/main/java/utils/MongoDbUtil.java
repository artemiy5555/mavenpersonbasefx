package utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import model.Person;


import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.pojo.PojoCodecProvider;

public class MongoDbUtil {
    private static final String Mongo_uri = "mongodb+srv://BorisM:111974@mongodb.svklc.mongodb.net/test";

    public static MongoCollection<Person> getDBConnection() {
        MongoCollection<Person> collection = new MongoClient(new MongoClientURI(Mongo_uri))
                .getDatabase("MongoDB")
                .withCodecRegistry(CodecRegistries.fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build())
                ))
                .getCollection("Persons", Person.class);


        System.out.println("Ready");


        return collection;
    }
}

package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlUtil {

    private static final String USER = "root";
    private static final String PASS = "root";

    private static final String URL_MYSQL =
            "jdbc:mysql://34.67.123.43:3306/new_schema?useSSL=false";

    public static Connection getDBConnection() {
        try {
            Connection dbConnection =
                    DriverManager.getConnection(URL_MYSQL, USER,PASS );
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}

package service.impl;

import dao.impl.MySqlDao;
import model.Person;
import service.HelpersService;
import service.PersonsService;

import java.util.List;

public class MySqlService implements PersonsService, HelpersService {

    private MySqlDao mySqlDao = new MySqlDao();

    @Override
    public void create(Person user) {
        mySqlDao.create(user);
    }

    @Override
    public void delete(int id) {
        mySqlDao.delete(id);
    }

    @Override
    public void update(Person user) {
        mySqlDao.update(user);
    }

    @Override
    public List read() {
        return mySqlDao.read();
    }

    @Override
    public void deleteAll(){ mySqlDao.deleteAll();}

    @Override
    public void deleteAllbyName(String name) { mySqlDao.deleteAllbyName(name); }

    @Override
    public void deleteAllbyLastName(String lname) { mySqlDao.deleteAllbyName(lname); }

    @Override
    public void deleteAllbyAge(int age) { mySqlDao.deleteAllbyAge(age); }

    @Override
    public void deleteAllbyCity(String city) { mySqlDao.deleteAllbyCity(city); }

    @Override
    public Person getById(int id) { return mySqlDao.getById(id); }

    @Override
    public List<Person> getByName(String name) { return mySqlDao.getByName(name); }

    @Override
    public List<Person> getByLastName(String lname) { return mySqlDao.getByLastName(lname);}

    @Override
    public List<Person> getByAge(int age) { return mySqlDao.getByAge(age); }

    @Override
    public List<Person> getByCity(String city) { return mySqlDao.getByCity(city); }


}

package service.impl;

import dao.impl.HibernateDao;
import model.Person;
import service.HelpersService;
import service.PersonsService;

import java.util.List;

public class HibernateService implements PersonsService, HelpersService {

    private HibernateDao hibernateDao = new HibernateDao();

    @Override
    public void create(Person user) {
        hibernateDao.create(user);
    }

    @Override
    public void delete(int id) {
        hibernateDao.delete(id);
    }

    @Override
    public void update(Person user) {
        hibernateDao.update(user);
    }

    @Override
    public List read() {
        return hibernateDao.read();
    }

    @Override
    public void deleteAll(){ hibernateDao.deleteAll();}

    @Override
    public void deleteAllbyName(String name) { hibernateDao.deleteAllbyName(name); }

    @Override
    public void deleteAllbyLastName(String lname) { hibernateDao.deleteAllbyName(lname); }

    @Override
    public void deleteAllbyAge(int age) { hibernateDao.deleteAllbyAge(age); }

    @Override
    public void deleteAllbyCity(String city) { hibernateDao.deleteAllbyCity(city); }

    @Override
    public Person getById(int id) { return hibernateDao.getById(id); }

    @Override
    public List<Person> getByName(String name) { return hibernateDao.getByName(name); }

    @Override
    public List<Person> getByLastName(String lname) { return hibernateDao.getByLastName(lname);}

    @Override
    public List<Person> getByAge(int age) { return hibernateDao.getByAge(age); }

    @Override
    public List<Person> getByCity(String city) { return hibernateDao.getByCity(city); }
}

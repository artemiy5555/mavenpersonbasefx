package service.impl;

import dao.impl.H2Dao;
import model.Person;
import service.HelpersService;
import service.PersonsService;

import java.util.List;

public class H2Service implements PersonsService, HelpersService {

    private H2Dao h2Dao = new H2Dao();

    @Override
    public void create(Person user) {
        h2Dao.create(user);
    }

    @Override
    public void delete(int id) {
        h2Dao.delete(id);
    }

    @Override
    public void update(Person user) {
        h2Dao.update(user);
    }

    @Override
    public List read() {
        return h2Dao.read();
    }

    @Override
    public void deleteAll(){ h2Dao.deleteAll();}

    @Override
    public void deleteAllbyName(String name) { h2Dao.deleteAllbyName(name); }

    @Override
    public void deleteAllbyLastName(String lname) { h2Dao.deleteAllbyName(lname); }

    @Override
    public void deleteAllbyAge(int age) { h2Dao.deleteAllbyAge(age); }

    @Override
    public void deleteAllbyCity(String city) { h2Dao.deleteAllbyCity(city); }

    @Override
    public Person getById(int id) { return h2Dao.getById(id); }

    @Override
    public List<Person> getByName(String name) { return h2Dao.getByName(name); }

    @Override
    public List<Person> getByLastName(String lname) { return h2Dao.getByLastName(lname);}

    @Override
    public List<Person> getByAge(int age) { return h2Dao.getByAge(age); }

    @Override
    public List<Person> getByCity(String city) { return h2Dao.getByCity(city); }


}

package service.impl;

import dao.impl.PostgresDao;
import model.Person;
import service.HelpersService;
import service.PersonsService;

import java.util.ArrayList;
import java.util.List;

public class PostgresSqlService implements PersonsService, HelpersService {

    private PostgresDao postgresDao =new PostgresDao();

    @Override
    public void create(Person user) {
        postgresDao.create(user);
    }

    @Override
    public void delete(int id) {
        postgresDao.delete(id);
    }

    @Override
    public void update(Person user) {
        postgresDao.update(user);
    }

    @Override
    public List read() {
        return postgresDao.read();
    }

    @Override
    public void deleteAll(){ postgresDao.deleteAll();}

    @Override
    public void deleteAllbyName(String name) { postgresDao.deleteAllbyName(name); }

    @Override
    public void deleteAllbyLastName(String lname) { postgresDao.deleteAllbyName(lname); }

    @Override
    public void deleteAllbyAge(int age) { postgresDao.deleteAllbyAge(age); }

    @Override
    public void deleteAllbyCity(String city) { postgresDao.deleteAllbyCity(city); }

    @Override
    public Person getById(int id) { return postgresDao.getById(id); }

    @Override
    public List<Person> getByName(String name) { return postgresDao.getByName(name); }

    @Override
    public List<Person> getByLastName(String lname) { return postgresDao.getByLastName(lname);}

    @Override
    public List<Person> getByAge(int age) { return postgresDao.getByAge(age); }

    @Override
    public List<Person> getByCity(String city) { return postgresDao.getByCity(city); }

}

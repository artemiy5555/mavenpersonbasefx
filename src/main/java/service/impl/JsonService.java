package service.impl;

import model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JsonService extends BinaryService{

    private final String JSON ="person.json";
    private static boolean emptyFile = false;

    public void jsonService() {
        File yourFile = new File(JSON);
        try {
            yourFile.createNewFile();
            new FileOutputStream(yourFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Person p) {//проверить
        String oldData = parseOldBase();

        try {
            PrintWriter writer = new PrintWriter(JSON);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter writer = new FileWriter(JSON, true);
            List<Person> person = Arrays.asList(p);
            for (Person per : person) {
                writer.write(writeLine(per, oldData));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List read() {
        ArrayList<Person> person = new ArrayList<>();
        String line = "";
        String jsonSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(JSON))) {

            line = br.readLine();

            if (line != null) {
                line = line.replaceAll("[\\[\\]{}\"]", "");
                String[] split = line.split(jsonSplitBy);

                for (String paramPerson : split) {
                    Person persons = new Person();
                    if (paramPerson.contains("id")) {
                        paramPerson = getValue(paramPerson);
                        persons.setId(Integer.parseInt(paramPerson));
                        continue;
                    }
                    if (paramPerson.contains("firstname")) {
                        paramPerson = getValue(paramPerson);
                        persons.setName(paramPerson);
                        continue;
                    }
                    if (paramPerson.contains("lname")) {
                        paramPerson = getValue(paramPerson);
                        persons.setLname(paramPerson);
                        continue;
                    }
                    if (paramPerson.contains("age")) {
                        paramPerson = getValue(paramPerson);
                        persons.setAge(Integer.parseInt(paramPerson));
                        continue;
                    }
                    if (paramPerson.contains("city")) {
                        paramPerson = getValue(paramPerson);
                        persons.setCity(paramPerson);


                        person.add(persons);
                    }
                }

            }

        } catch (IOException e) {}

        return person;
    }

    @Override
    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(JSON);
            writer.write("");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String writeLine(Person p, String oldData){

        String currentPerson = personToString(p);

        if (!emptyFile)
            currentPerson = oldData.substring(0, oldData.length() - 1) + "," + currentPerson + "]";
        else
            currentPerson = "[" + currentPerson + "]";

        return currentPerson;
    }

    private String personToString(Person person) {
        return "{"
                + "\"id\" : \"" + person.getId() + "\", "
                + "\"firstname\" : \"" + person.getName() + "\", "
                + "\"lname\" : \"" + person.getLname() + "\", "
                + "\"age\" : \"" + person.getAge() + "\", "
                + "\"city\" : \"" + person.getCity() + "\"}";
    }

    private String parseOldBase(){

        String personsOldData = "";

        try (BufferedReader br = new BufferedReader(new FileReader(JSON))) {
            if ((personsOldData = br.readLine()) == null) {
                emptyFile = true;
                String newPersonsOldData = "";
                return newPersonsOldData;
            }else {emptyFile = false;}
        } catch (IOException e) {
            e.printStackTrace();
        }

        return personsOldData;
    }

    private String getValue(String str) {
        return str.substring(str.indexOf(":") + 1).trim();
    }

}

package service.impl;

import model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CsvService extends BinaryService{

    private final String csv ="person.csv";

    public void csvService() {
        File yourFile = new File(csv);
        try {
            yourFile.createNewFile();
            new FileOutputStream(yourFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Person p) {//проверить

        try {
            FileWriter writer = new FileWriter(csv, true);
            List<Person> person = Collections.singletonList(p);

            for (Person per : person) {
                List<String> list = new ArrayList<>();
                list.add(String.valueOf(per.getId()));
                list.add(per.getName());
                list.add(per.getLname());
                list.add(String.valueOf(per.getAge()));
                list.add(per.getCity());
                writeLine(writer, list);
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeLine(Writer w, List<String> values) throws IOException {

        char separators = ',';
        char customQuote = ' ';
        boolean first = true;

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(separators);
            }
            sb.append(followCVSformat(value));

            first = false;
        }
        sb.append("\n");
        w.append(sb.toString());
    }

    private String followCVSformat(String value) {

        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    @Override
    public List read() {
        String line;
        String cvsSplitBy = ",";
        ArrayList<Person> person = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csv))) {

            while ((line = br.readLine()) != null) {
                String[] persons = line.split(cvsSplitBy);
                person.add(new Person(Integer.parseInt(persons[0]), persons[1], persons[2], Integer.parseInt(persons[3]), persons[4]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }

    @Override
    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(csv);
            writer.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

package service.impl;

import model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XmlService extends BinaryService {

    public static boolean emptyFile;
    private final String XML ="person.xml";

    public void xmlService() {
        File yourFile = new File(XML);
        try {
            yourFile.createNewFile();
            new FileOutputStream(yourFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Person p) {//проверить
        String oldData = parseOldBase();

        try {
            PrintWriter writer = new PrintWriter(XML);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter writer = new FileWriter(XML, true);
            List<Person> person = Arrays.asList(p);
            for (Person per : person) {
                writer.write(writeLine(per, oldData));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
        }
    }

    private String parseOldBase() {

        String personsOldData = "";
        String newersonsOldData = "";

        try (BufferedReader br = new BufferedReader(new FileReader(XML))) {
            emptyFile = true;

            while ((personsOldData = br.readLine()) != null) {

                newersonsOldData = newersonsOldData +personsOldData+"\n";
                emptyFile = false;
            }
            return newersonsOldData;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return personsOldData;
    }

    private String writeLine(Person p, String oldData) throws IOException {

        String currentPerson = personToString(p);

        if (!emptyFile) {
            currentPerson = oldData.substring(0, oldData.length() - 11) + currentPerson+"\n</Persons>";
        } else currentPerson = "<Persons>\n" + currentPerson + "\n</Persons>";

        return currentPerson;
    }

    @Override
    public List read() {
        ArrayList<Person> person = new ArrayList<>();
        String line = "";
        String XMLSplitBy = ": ";
        String split = "";
        try (BufferedReader br = new BufferedReader(new FileReader(XML))) {
            Person persons = new Person();
            while ((line = br.readLine()) != null) {

                try {
                    split = line.substring(line.indexOf(">") + 1, line.lastIndexOf("<"));
                } catch (Exception e) {
                }

                if (line.contains("id"))
                    persons.setId(Integer.parseInt(split));
                if (line.contains("firstname"))
                    persons.setName(split);
                if (line.contains("lname"))
                    persons.setLname(split);
                if (line.contains("age"))
                    persons.setAge(Integer.parseInt(split));
                if (line.contains("city")) {
                    persons.setCity(split);
                    Person newPersons = new Person();
                    newPersons = newPersons.copy(persons);
                    person.add(newPersons);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }

    @Override
    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(XML);
            writer.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String personToString(Person person) {
        return "<Person>"
                + "\n<id>" + person.getId() + "</id>"
                + "\n<firstname>" + person.getName() + "</firstname>"
                + "\n<lname>" + person.getLname() + "</lname>"
                + "\n<age>" + person.getAge() + "</age>"
                + "\n<city>" + person.getCity() + "</city>"
                + "\n</Person>";
    }

}

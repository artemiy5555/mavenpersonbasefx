package service.impl;

import model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YamlService extends BinaryService{

    private final String YAML ="person.yaml";

    public void yamlService() {
        File yourFile = new File(YAML);
        try {
            yourFile.createNewFile();
            new FileOutputStream(yourFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Person p) {//проверить
        String dataSave = dataSave();
        try {
            FileWriter writer = new FileWriter(YAML, true);
            List<Person> person = Arrays.asList(p);
            for (Person per : person) {
                writer.write(personToConsole(per));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List read() {
        ArrayList<Person> person =new ArrayList<>();
        String line ="";
        String yamlSplitBy = ": ";

        try (BufferedReader br = new BufferedReader(new FileReader(YAML))) {
            Person persons = new Person();
            while ((line = br.readLine()) != null) {
                String[]split = line.split(yamlSplitBy);

                if (line.contains("id")) {
                    persons.setId(Integer.parseInt(split[1]));
                    continue;
                }
                if (line.contains("first")){
                    persons.setName(split[1]);
                    continue;
                }
                if (line.contains("second")){
                    persons.setLname(split[1]);
                    continue;
                }
                if (line.contains("age")){
                    persons.setAge(Integer.parseInt(split[1]));
                    continue;
                }
                if (line.contains("city")) {
                    persons.setCity(split[1]);
                    Person newPersons = new Person();
                    newPersons = newPersons.copy(persons);
                    person.add(newPersons);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }

    @Override
    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(YAML);
            writer.write("persons:\n  ");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String dataSave() {
        File file = new File(YAML);
        String personDataSave = " ";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            {
                personDataSave = br.readLine();
            }
        } catch (IOException e) {
        }
        return personDataSave;
    }

    private String personToConsole(Person person) {
        String construct = "persona" + person.getId() + ":"
                + "\n\t - id : " + person.getId()
                + "\n\t - first name : " + person.getName()
                + "\n\t - second name : " + person.getLname()
                + "\n\t - age : " + person.getAge()
                + "\n\t - city : " + person.getCity() + "\n  ";

        return construct;
    }
}

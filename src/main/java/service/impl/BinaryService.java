package service.impl;

import model.Person;
import service.HelpersService;
import service.PersonsService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BinaryService implements PersonsService,HelpersService {

    private final String bin ="person.bin";

    List<Person> people ;

    public void binaryService() {
        new File(bin);
    }

    @Override
    public void create(Person p) {
        try(DataOutputStream dos = new DataOutputStream(new FileOutputStream(bin,true))) {
            dos.writeInt(p.getId());
            dos.writeUTF(p.getName());
            dos.writeUTF(p.getLname());
            dos.writeInt(p.getAge());
            dos.writeUTF(p.getCity());
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public List read() {
        List<Person> persons = new ArrayList<>();
        try(DataInputStream dos = new DataInputStream(new FileInputStream(bin))) {

            while (true) {
                persons.add(new Person(dos.readInt(),dos.readUTF(),dos.readUTF(),dos.readInt(),dos.readUTF()));
            }

        }
        catch(IOException e){
            //e.printStackTrace();
        }
        return persons;
    }

    @Override
    public void update(Person p) {
        people = read();

        for (Person person : people) {
            if (person.getId().equals(p.getId())) {
                person.setName(p.getName());
                person.setLname(p.getLname());
                person.setAge(p.getAge());
                person.setCity(p.getCity());
            }
        }
        deleteAll();
        saveAllBin(people);
    }

    @Override
    public void delete(int id) {
        people = read();
        List<Person> personsByName = new ArrayList<>();
        for (Person person : people) {
            if (!person.getId().equals(id)){
                personsByName.add(person);
            }

        }
        people.clear();
        deleteAll();
        saveAllBin(personsByName);
    }

    private void saveAllBin(List<Person> persons) {
        for (Person person : persons)
            create(person);
    }


    @Override
    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(bin);
            writer.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllbyName(String name) {
        people = read();
        List<Person> personsByName = new ArrayList<>();
        for (Person person : people) {
            if (!person.getName().equals(name)){
                personsByName.add(person);
            }

        }
        people.clear();
        deleteAll();
        saveAllBin(personsByName);
    }

    @Override
    public void deleteAllbyLastName(String lname) {
        people = read();
        List<Person> personsByName = new ArrayList<>();
        for (Person person : people) {
            if (!person.getLname().equals(lname)){
                personsByName.add(person);
            }

        }
        people.clear();
        deleteAll();
        saveAllBin(personsByName);
    }

    @Override
    public void deleteAllbyAge(int age) {
        people = read();
        List<Person> personsByName = new ArrayList<>();
        for (Person person : people) {
            if (person.getAge()!=age){
                personsByName.add(person);
            }

        }
        people.clear();
        deleteAll();
        saveAllBin(personsByName);
    }

    @Override
    public void deleteAllbyCity(String city) {
        people = read();
        List<Person> personsByName = new ArrayList<>();
        for (Person person : people) {
            if (!person.getCity().equals(city)){
                personsByName.add(person);
            }

        }
        people.clear();
        deleteAll();
        saveAllBin(personsByName);
    }

    @Override
    public Person getById(int id) {
        people = read();
        Person personById = null;
        for (Person person : people) {
            if (person.getId() == id)
                personById= person;
        }
        people.clear();
        return personById;
    }

    @Override
    public List<Person> getByName(String name) {
        people = read();
        List<Person> personsByName = new ArrayList<>();
        for (Person person : people) {
            if (person.getName().equals(name)){
                personsByName.add(person);
            }

        }
        people.clear();
        return personsByName;
    }

    @Override
    public List<Person> getByLastName(String lname) {
        people = read();
        List<Person> personsByLastName = new ArrayList<>();
        for (Person person : people) {
            if (person.getLname().equals(lname)){
                personsByLastName.add(person);
            }

        }
        people.clear();
        return personsByLastName;
    }

    @Override
    public List<Person> getByAge(int age) {
        people = read();
        List<Person> personsByAge = new ArrayList<>();
        for (Person person : people) {
            if (person.getAge().equals(age)){
                personsByAge.add(person);
            }

        }
        people.clear();
        return personsByAge;
    }

    @Override
    public List<Person> getByCity(String city) {
        people = read();
        List<Person> personsByCity = new ArrayList<>();
        for (Person person : people) {
            if (person.getCity().equals(city)){
                personsByCity.add(person);
            }

        }
        people.clear();
        return personsByCity;
    }
}

package service;

import model.Person;

import java.util.ArrayList;
import java.util.List;

public interface PersonsService {

    void create(Person p);

    List read();

    void update(Person p);

    void delete(int id);

}

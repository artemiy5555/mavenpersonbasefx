package service;

import model.Person;

import java.util.ArrayList;
import java.util.List;

public interface HelpersService {

    void deleteAll();

    void deleteAllbyName(String name);

    void deleteAllbyLastName(String lname);

    void deleteAllbyAge(int age);

    void deleteAllbyCity(String city);

    Person getById(int id);

    List<Person> getByName(String name);

    List<Person> getByLastName(String lname);

    List<Person> getByAge(int age);

    List<Person> getByCity(String city);

}

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import utils.HibernateSessionFactoryUtil;

public class MainFx extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Base");
        Parent view = FXMLLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        HibernateSessionFactoryUtil.closeSessionFactory();
        System.out.println("Application stops");
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

}

import dao.impl.H2Dao;
import dao.impl.MongoDAO;
import dao.impl.Neo4jDao;
import model.Person;
import service.impl.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class AllGRUD {

    private PostgresSqlService postgresSqlService = new PostgresSqlService();
    private MySqlService serviceMySql = new MySqlService();
    private HibernateService hibernateService = new HibernateService();
    private H2Dao h2Dao = new H2Dao();
    private BinaryService binaryService = new BinaryService();
    private CsvService csvService = new CsvService();
    private YamlService yamlService = new YamlService();
    private XmlService xmlService = new XmlService();
    private MongoDAO mongoService = new MongoDAO();
    private JsonService jsonService = new JsonService();
    private Neo4jDao neo4jService= new Neo4jDao();

    List getAll(String valueBase) {
        List people = null;
        switch (valueBase) {
            case "H2":
                people = h2Dao.read();
                break;
            case "MySQL":
                people = serviceMySql.read();
                break;
            case "PostgreSQL":
                people = postgresSqlService.read();
                break;
            case "MySQL_Hibernate":
                people = hibernateService.read();
                break;
            case "BINARY":
                people = binaryService.read();
                break;
            case "CSV":
                people = csvService.read();
                break;
            case "XML":
                people = xmlService.read();
                break;
            case "YAML":
                people = yamlService.read();
                break;
            case "Mongo":
                people = mongoService.read();
                break;
            case "JSON":
                people = jsonService.read();
                break;
            case "OrientDb":
                people = neo4jService.read();
                break;
        }
        return people;
    }

    void insert(Person person, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.create(person);
                break;
            case "MySQL":
                serviceMySql.create(person);
                break;
            case "PostgreSQL":
                postgresSqlService.create(person);
                break;
            case "MySQL_Hibernate":
                hibernateService.create(person);
                break;
            case "BINARY":
                person.setId(createID(getAll(valueBase)));
                binaryService.create(person);
                break;
            case "CSV":
                person.setId(createID(getAll(valueBase)));
                csvService.create(person);
                break;
            case "XML":
                person.setId(createID(getAll(valueBase)));
                xmlService.create(person);
                break;
            case "YAML":
                person.setId(createID(getAll(valueBase)));
                yamlService.create(person);
                break;
            case "JSON":
                person.setId(createID(getAll(valueBase)));
                jsonService.create(person);
                break;
            case "Mongo":
                person.setId(createID(getAll(valueBase)));
                mongoService.create(person);
                break;
            case "OrientDb":
                person.setId(createID(getAll(valueBase)));
                neo4jService.create(person);
                break;
        }
    }

    void deleteAll(String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.deleteAll();
                break;
            case "MySQL":
                serviceMySql.deleteAll();
            case "PostgreSQL":
                postgresSqlService.deleteAll();
                break;
            case "MySQL_Hibernate":
                hibernateService.deleteAll();
                break;
            case "BINARY":
                binaryService.deleteAll();
                break;
            case "CSV":
                csvService.deleteAll();
                break;
            case "XML":
                xmlService.deleteAll();
                break;
            case "YAML":
                yamlService.deleteAll();
                break;
            case "JSON":
                jsonService.deleteAll();
                break;
            case "Mongo":
                mongoService.deleteAll();
                break;
            case "OrientDb":
                neo4jService.deleteAll();
                break;
        }
    }

    void deleteById(int id, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.delete(id);
                break;
            case "MySQL":
                serviceMySql.delete(id);
                break;
            case "PostgreSQL":
                postgresSqlService.delete(id);
                break;
            case "MySQL_Hibernate":
                hibernateService.delete(id);
                break;
            case "BINARY":
                binaryService.delete(id);
                break;
            case "CSV":
                csvService.delete(id);
                break;
            case "XML":
                xmlService.delete(id);
                break;
            case "YAML":
                yamlService.delete(id);
                break;
            case "JSON":
                jsonService.delete(id);
                break;
            case "Mongo":
                mongoService.delete(id);
                break;
            case "OrientDb":
                neo4jService.delete(id);
                break;
        }
    }

    void update(Person person, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.update(person);
                break;
            case "MySQL":
                serviceMySql.update(person);
                break;
            case "PostgreSQL":
                postgresSqlService.update(person);
                break;
            case "MySQL_Hibernate":
                hibernateService.update(person);
                break;
            case "BINARY":
                binaryService.update(person);
                break;
            case "CSV":
                csvService.update(person);
                break;
            case "XML":
                xmlService.update(person);
                break;
            case "YAML":
                yamlService.update(person);
                break;
            case "JSON":
                jsonService.update(person);
                break;
            case "Mongo":
                mongoService.update(person);
                break;
            case "OrientDb":
                neo4jService.update(person);
                break;
        }
    }

    void deleteAllByName(String name, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.deleteAllbyName(name);
                break;
            case "MySQL":
                serviceMySql.deleteAllbyName(name);
                break;
            case "PostgreSQL":
                postgresSqlService.deleteAllbyName(name);
                break;
            case "MySQL_Hibernate":
                hibernateService.deleteAllbyName(name);
                break;
            case "BINARY":
                binaryService.deleteAllbyName(name);
                break;
            case "CSV":
                csvService.deleteAllbyName(name);
                break;
            case "XML":
                xmlService.deleteAllbyName(name);
                break;
            case "YAML":
                yamlService.deleteAllbyName(name);
                break;
            case "JSON":
                jsonService.deleteAllbyName(name);
                break;
            case "Mongo":
                mongoService.deleteAllbyName(name);
                break;
            case "OrientDb":
                neo4jService.deleteAllbyName(name);
                break;
        }
    }

    void deleteAllbyLastName(String lastName, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.deleteAllbyLastName(lastName);
                break;
            case "MySQL":
                serviceMySql.deleteAllbyLastName(lastName);
                break;
            case "PostgreSQL":
                postgresSqlService.deleteAllbyLastName(lastName);
                break;
            case "MySQL_Hibernate":
                hibernateService.deleteAllbyLastName(lastName);
                break;
            case "BINARY":
                binaryService.deleteAllbyLastName(lastName);
                break;
            case "CSV":
                csvService.deleteAllbyLastName(lastName);
                break;
            case "XML":
                xmlService.deleteAllbyLastName(lastName);
                break;
            case "YAML":
                yamlService.deleteAllbyLastName(lastName);
                break;
            case "JSON":
                jsonService.deleteAllbyLastName(lastName);
                break;
            case "Mongo":
                mongoService.deleteAllbyLastName(lastName);
                break;
            case "OrientDb":
                neo4jService.deleteAllbyLastName(lastName);
                break;
        }
    }

    void deleteAllbyAge(int age, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.deleteAllbyAge(age);
                break;
            case "MySQL":
                serviceMySql.deleteAllbyAge(age);
                break;
            case "PostgreSQL":
                postgresSqlService.deleteAllbyAge(age);
                break;
            case "MySQL_Hibernate":
                hibernateService.deleteAllbyAge(age);
                break;
            case "BINARY":
                binaryService.deleteAllbyAge(age);
                break;
            case "CSV":
                csvService.deleteAllbyAge(age);
                break;
            case "XML":
                xmlService.deleteAllbyAge(age);
                break;
            case "YAML":
                yamlService.deleteAllbyAge(age);
                break;
            case "JSON":
                jsonService.deleteAllbyAge(age);
                break;
            case "Mongo":
                mongoService.deleteAllbyAge(age);
                break;
            case "OrientDb":
                neo4jService.deleteAllbyAge(age);
                break;
        }
    }

    void deleteAllbyCity(String city, String valueBase) {
        switch (valueBase) {
            case "H2":
                h2Dao.deleteAllbyCity(city);
                break;
            case "MySQL":
                serviceMySql.deleteAllbyCity(city);
                break;
            case "PostgreSQL":
                postgresSqlService.deleteAllbyCity(city);
                break;
            case "MySQL_Hibernate":
                hibernateService.deleteAllbyCity(city);
                break;
            case "BINARY":
                binaryService.deleteAllbyCity(city);
                break;
            case "CSV":
                csvService.deleteAllbyCity(city);
                break;
            case "XML":
                xmlService.deleteAllbyCity(city);
                break;
            case "YAML":
                yamlService.deleteAllbyCity(city);
                break;
            case "JSON":
                jsonService.deleteAllbyCity(city);
                break;
            case "Mongo":
                mongoService.deleteAllbyCity(city);
                break;
            case "OrientDb":
                neo4jService.deleteAllbyCity(city);
                break;
        }
    }

    List getById(int id, String valueBase) {
        List people = new ArrayList(1);
        switch (valueBase) {
            case "H2":
                people.add(h2Dao.getById(id));
                break;
            case "MySQL":
                people.add(serviceMySql.getById(id));
                break;
            case "PostgreSQL":
                people.add(postgresSqlService.getById(id));
                break;
            case "MySQL_Hibernate":
                people.add(hibernateService.getById(id));
                break;
            case "BINARY":
                people.add(binaryService.getById(id));
                break;
            case "CSV":
                people.add(csvService.getById(id));
                break;
            case "XML":
                people.add(xmlService.getById(id));
                break;
            case "YAML":
                people.add(yamlService.getById(id));
                break;
            case "JSON":
                people.add(jsonService.getById(id));
                break;
            case "Mongo":
                people.add(mongoService.getById(id));
                break;
            case "OrientDb":
                people.add(neo4jService.getById(id));
                break;
        }
        return people;
    }

    List getByName(String name, String valueBase) {
        List people = null;
        switch (valueBase) {
            case "H2":
                people = h2Dao.getByName(name);
                break;
            case "MySQL":
                people = serviceMySql.getByName(name);
                break;
            case "PostgreSQL":
                people = postgresSqlService.getByName(name);
                break;
            case "MySQL_Hibernate":
                people = hibernateService.getByName(name);
                break;
            case "BINARY":
                people = binaryService.getByName(name);
                break;
            case "CSV":
                people = csvService.getByName(name);
                break;
            case "XML":
                people = xmlService.getByName(name);
                break;
            case "YAML":
                people = yamlService.getByName(name);
                break;
            case "JSON":
                people = jsonService.getByName(name);
                break;
            case "Mongo":
                people = mongoService.getByName(name);
                break;
            case "OrientDb":
                people = neo4jService.getByName(name);
                break;
        }
        return people;
    }

    List getByLastName(String lastName, String valueBase) {
        List people = null;
        switch (valueBase) {
            case "H2":
                people = h2Dao.getByLastName(lastName);
                break;
            case "MySQL":
                people = serviceMySql.getByLastName(lastName);
                break;
            case "PostgreSQL":
                people = postgresSqlService.getByLastName(lastName);
                break;
            case "MySQL_Hibernate":
                people = hibernateService.getByLastName(lastName);
                break;
            case "BINARY":
                people = binaryService.getByLastName(lastName);
                break;
            case "CSV":
                people = csvService.getByLastName(lastName);
                break;
            case "XML":
                people = xmlService.getByLastName(lastName);
                break;
            case "YAML":
                people = yamlService.getByLastName(lastName);
                break;
            case "JSON":
                people = jsonService.getByLastName(lastName);
                break;
            case "Mongo":
                people = mongoService.getByLastName(lastName);
                break;
            case "OrientDb":
                people = neo4jService.getByLastName(lastName);
                break;
        }
        return people;
    }

    List getByAge(int age, String valueBase) {
        List people = null;
        switch (valueBase) {
            case "H2":
                people = h2Dao.getByAge(age);
                break;
            case "MySQL":
                people = serviceMySql.getByAge(age);
                break;
            case "PostgreSQL":
                people = postgresSqlService.getByAge(age);
                break;
            case "MySQL_Hibernate":
                people = hibernateService.getByAge(age);
                break;
            case "BINARY":
                people = binaryService.getByAge(age);
                break;
            case "CSV":
                people = csvService.getByAge(age);
                break;
            case "XML":
                people = xmlService.getByAge(age);
                break;
            case "YAML":
                people = yamlService.getByAge(age);
                break;
            case "JSON":
                people = jsonService.getByAge(age);
                break;
            case "Mongo":
                people = mongoService.getByAge(age);
                break;
            case "OrientDb":
                people = neo4jService.getByAge(age);
                break;
        }
        return people;
    }

    List getByCity(String city, String valueBase) {
        List people = null;
        switch (valueBase) {
            case "H2":
                people = h2Dao.getByCity(city);
                break;
            case "MySQL":
                people = serviceMySql.getByCity(city);
                break;
            case "PostgreSQL":
                people = postgresSqlService.getByCity(city);
                break;
            case "MySQL_Hibernate":
                people = hibernateService.getByCity(city);
                break;
            case "BINARY":
                people = binaryService.getByCity(city);
                break;
            case "CSV":
                people = csvService.getByCity(city);
                break;
            case "XML":
                people = xmlService.getByCity(city);
                break;
            case "YAML":
                people = yamlService.getByCity(city);
                break;
            case "JSON":
                people = jsonService.getByCity(city);
                break;
            case "Mongo":
                people = mongoService.getByCity(city);
                break;
            case "OrientDb":
                people = neo4jService.getByCity(city);
                break;
        }
        return people;
    }

    private static int createID(List<Person> persons) {

        char[] chars = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder(7);
        Random random = new Random();

        for (int i = 0; i < 7; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        String output = sb.toString();
        int id = Integer.parseInt(output);

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getId() == id) {
                id = createID(persons);
            }
        }
        return id;
    }
}
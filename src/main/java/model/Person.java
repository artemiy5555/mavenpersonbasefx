package model;

import javax.persistence.*;

@Entity
@Table(name = "persons")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "LNAME")
    private String lname;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "CITY")
    private String city;

    public Person() {
    }

    public Person(String name, String lname, Integer age, String city) {
        this.name = name;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Person(Integer id, String name, String lname, Integer age, String city) {
        this.id = id;
        this.name = name;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    @Override
    public String toString() {
        return "model.Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Person copy(Person persons) {

        Person newPersons = new Person(persons.id, persons.name, persons.lname, persons.age, persons.city);

        return newPersons;
    }
}

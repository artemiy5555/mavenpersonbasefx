package dao.impl;

import dao.HelpersDao;
import dao.PersonsDao;
import model.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class HibernateDao implements PersonsDao, HelpersDao {

    @Override
    public void create(Person person) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(person);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Person person) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(person);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Person WHERE id = :id")
                .setParameter("id", id)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    @Override
    public void deleteAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        List<Person> people = read();
        for (Person person: people)
        session.delete(person);
        tx1.commit();
        session.close();
    }

    @Override
    public void deleteAllbyName(String name) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Person WHERE name = :name")
        .setParameter("name", name)
        .executeUpdate();
                tx1.commit();
        session.close();
    }

    @Override
    public void deleteAllbyLastName(String lname) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Person WHERE lname = :lname")
                .setParameter("lname", lname)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    @Override
    public void deleteAllbyAge(int age) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Person WHERE age = :age")
                .setParameter("age", age)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    @Override
    public void deleteAllbyCity(String city) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Person WHERE city = :city")
                .setParameter("city", city)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    @Override
    public Person getById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession()
                .get(Person.class, id);
    }

    @Override
    public List<Person> getByName(String name) {

        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Person> list =
                 session
                .createQuery("FROM Person WHERE name = :name")
                .setParameter("name", name)
                .list();
        session.close();
        return list;
    }

    @Override
    public List<Person> getByLastName(String lname) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        ArrayList<Person> arrayList =
                (ArrayList<Person>) session
                        .createQuery("FROM Person WHERE lname = :lname")
                        .setParameter("lname", lname)
                        .list();
        session.close();
        System.out.println(arrayList);
        return arrayList;
    }

    @Override
    public List<Person> getByAge(int age) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            List<Person> list =
                    session
                        .createQuery("FROM Person WHERE age = :age")
                        .setParameter("age", age)
                        .list();
        session.close();
        return list;
    }

    @Override
    public List<Person> getByCity(String city) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Person> list =
                (ArrayList<Person>) session
                        .createQuery("FROM Person WHERE city = :city")
                        .setParameter("city", city)
                        .list();
        session.close();
        return list;
    }

    public List read() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Person> list =
                (ArrayList<Person>) session
                        .createQuery("FROM Person")
                        .list();
        session.close();
        return list;
    }

}

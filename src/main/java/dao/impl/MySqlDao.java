package dao.impl;


import utils.MySqlUtil;

import java.sql.*;


public class MySqlDao extends SqlDao {

    public void mySqlCreateTable() {
        try{
            Connection conn = getDBConnection();
            Statement stmt = conn.createStatement();
            String schema = "CREATE SCHEMA new_schema";
            stmt.executeUpdate(schema);
            String sql = "CREATE TABLE new_schema.persons(" +
                    "id auto_increment PRIMARY KEY," +
                    " NAME VARCHAR(255)," +
                    " LNAME VARCHAR(255)," +
                    " AGE INT," +
                    " CITY VARCHAR(255));";
            stmt.executeUpdate(sql);

        } catch(Exception ignored){}

    }

    public Connection getDBConnection() {
        return MySqlUtil.getDBConnection();
    }

}

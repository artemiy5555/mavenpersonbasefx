package dao.impl;

import utils.PostgresUtil;

import java.sql.Connection;
import java.sql.Statement;

public class PostgresDao extends SqlDao{

    public void postrgesCreateTable() {
        try{
            Connection conn = getDBConnection();
            Statement stmt = conn.createStatement();
            String schema ="CREATE SCHEMA new_schema";
            stmt.executeUpdate(schema);
            String sql = "CREATE TABLE new_schema.persons(" +
                    "id SERIAL PRIMARY KEY," +
                    " NAME VARCHAR(45)," +
                    " LNAME VARCHAR(45)," +
                    " AGE INT," +
                    " CITY VARCHAR(45));";
            stmt.executeUpdate(sql);
        } catch(Exception ignored){}
    }

    public Connection getDBConnection() {
        return PostgresUtil.getDBConnection();
    }


}

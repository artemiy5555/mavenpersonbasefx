package dao.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import dao.HelpersDao;
import dao.PersonsDao;
import model.Person;
import utils.MongoDbUtil;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MongoDAO implements PersonsDao, HelpersDao {

    @Override
    public void deleteAll() {
        MongoDbUtil.getDBConnection().deleteMany(new Document());
    }

    @Override
    public void deleteAllbyName(String firstName) {
        MongoDbUtil.getDBConnection().deleteMany(new Document("firstName", firstName));
    }

    @Override
    public void deleteAllbyLastName(String lastName) {
        MongoDbUtil.getDBConnection().deleteMany(new Document("lastName", lastName));
    }

    @Override
    public void deleteAllbyAge(int age) {
        MongoDbUtil.getDBConnection().deleteMany(new Document("age", age));
    }

    @Override
    public void deleteAllbyCity(String city) {
        MongoDbUtil.getDBConnection().deleteMany(new Document("city", city));
    }

    @Override
    public Person getById(int id) {
        return MongoDbUtil.getDBConnection().find(Filters.eq("age", id)).first();
    }

    @Override
    public List<Person> getByName(String name) {

        List<Person> persons = read();
        List<Person> sortedPersons = new ArrayList<>();

        for (Person person : persons) {
            if (person.getName().equals(name)) {
                sortedPersons.add(person);
            }
        }
        return sortedPersons;
    }

    @Override
    public List<Person> getByLastName(String lastName) {

        List<Person> persons = read();
        List<Person> sortedPersons = new ArrayList<>();

        for (Person person : persons) {
            if (person.getLname().equals(lastName)) {
                sortedPersons.add(person);
            }
        }
        return sortedPersons;
    }

    @Override
    public List<Person> getByAge(int age) {

        List<Person> persons = read();
        List<Person> sortedPersons = new ArrayList<>();

        for (Person person : persons) {
            if (person.getAge() == age) {
                sortedPersons.add(person);
            }
        }
        return sortedPersons;
    }

    @Override
    public List<Person> getByCity(String city) {

        List<Person> persons = read();
        List<Person> sortedPersons = new ArrayList<>();

        for (Person person : persons) {
            if (person.getCity().equals(city)) {
                sortedPersons.add(person);
            }
        }
        return sortedPersons;
    }

    @Override
    public void create(Person p) {
        List<Person> persons = read();
        p.setId(createID(persons));
        MongoDbUtil.getDBConnection().insertOne(p);
    }

    @Override
    public List<Person> read() {

        List<Person> list = new ArrayList<>();
        MongoCollection<Person> collection = MongoDbUtil.getDBConnection();

        MongoCursor<Person> cursor = collection.find().iterator();

        try {
            while (cursor.hasNext()) {
                list.add(cursor.next());
            }
        } finally {
            cursor.close();
        }
        return list;

    }

    @Override
    public void update(Person p) {

       /* Document document = new Document("id",id);
        document.put("age", age);
        document.put("city", city);
        document.put("firstName", firstName);
        document.put("lastName", lastName);
        MongoDbUtil.getDBConnection().updateOne(Filters.eq("iD",id),new Document("$set",document));*/

        Document document = new Document("id", p.getId());
        document.put("age", p.getAge());
        document.put("city", p.getAge());
        document.put("firstName", p.getName());
        document.put("lastName", p.getLname());
        MongoDbUtil.getDBConnection().updateOne(Filters.eq("iD", p.getId()), new Document("$set", document));
    }

    @Override
    public void delete(int id) {
        MongoDbUtil.getDBConnection().deleteOne(new Document("id", id));
    }

    private static int createID(List<Person> persons) {

        char[] chars = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder(7);
        Random random = new Random();

        for (int i = 0; i < 7; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        String output = sb.toString();
        int id = Integer.parseInt(output);

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getId() == id) {
                id = createID(persons);
            }
        }
        return id;
    }
}

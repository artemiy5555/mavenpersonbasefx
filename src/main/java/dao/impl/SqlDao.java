package dao.impl;

import dao.HelpersDao;
import dao.PersonsDao;
import model.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



public class SqlDao implements PersonsDao, HelpersDao {

    private List<Person> people = new ArrayList<>();

    {
        Locale.setDefault(Locale.ENGLISH);
    }

    public Connection getDBConnection() {
        return null;
    }
    private Connection c = getDBConnection();

    public void create(Person person) {

        try {
            assert c != null;
            PreparedStatement statement = c.prepareStatement(
                    "INSERT INTO new_schema.persons" +
                            "(NAME, LNAME, AGE,CITY)" +
                            "VALUES ( ?, ?, ?, ?)"
            );

            statement.setString(1, person.getName());
            statement.setString(2, person.getLname());
            statement.setInt(3, person.getAge());
            statement.setString(4, person.getCity());
            statement.execute();

            System.out.println("Создана запись: " + person);

            statement.clearParameters();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List read(){

        people.clear();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT * FROM new_schema.persons"
            );

            while (set.next()) {
                people.add(new Person(
                        set.getInt(1),
                        set.getString(2),
                        set.getString(3),
                        set.getInt(4),
                        set.getString(5)
                ));
            }
            statement.close();

            for (Person p : people) {
                System.out.println("Получена запись: " + p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return people;
    }

    public void delete(int id){

        try  {
            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM new_schema.persons WHERE ID = ?"
                    );
            statement.setInt(1, id);
            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Person newPerson) {

        try  {

            PreparedStatement statement =
                    c.prepareStatement(
                            "SELECT * FROM new_schema.persons WHERE ID = ?");
            statement.setInt(1, newPerson.getId());
            ResultSet set = statement.executeQuery();

            Person person = new Person();
            while (set.next()) {
                person.setId(set.getInt(1));
                person.setName(set.getString(2));
                person.setLname(set.getString(3));
                person.setAge(set.getInt(4));
                person.setCity(set.getString(5));
            }
            statement.close();

            System.out.println("Получена запись : " + person);
            person.setId(newPerson.getId());
            person.setName(newPerson.getName());
            person.setLname(newPerson.getLname());
            person.setAge(newPerson.getAge());
            person.setCity(newPerson.getCity());

            PreparedStatement upd = c.
                    prepareStatement(
                            "UPDATE new_schema.persons SET NAME = ?, LNAME = ?, AGE = ?, CITY = ? WHERE ID = ?");
            upd.setString(1, person.getName());
            upd.setString(2, person.getLname());
            upd.setInt(3, person.getAge());
            upd.setString(4,person.getCity());
            upd.setInt(5,person.getId());
            upd.execute();
            System.out.println("Обновленная запись: " + person);
            upd.close();

            System.out.println("Обновление прошло успешно!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll(){
        try  {

            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM new_schema.persons "
                    );
            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllbyName(String name){
        try  {

            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM new_schema.persons WHERE NAME = ?"
                    );
            statement.setString(1, name);
            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllbyLastName(String lname){
        try  {

            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM new_schema.persons WHERE LNAME = ?"
                    );
            statement.setString(1, lname);
            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllbyAge(int age){
        try  {

            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM new_schema.persons WHERE AGE = ?"
                    );
            statement.setInt(1, age);
            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllbyCity(String city){
        try  {

            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM new_schema.persons WHERE CITY = ?"
                    );
            statement.setString(1, city);
            statement.execute();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Person getById(int id){
        Person person = new Person();

        try  {

            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM new_schema.persons WHERE ID = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            person.setId(resultSet.getInt(1));
            person.setName(resultSet.getString(2));
            person.setLname(resultSet.getString(3));
            person.setAge(resultSet.getInt(4));
            person.setCity(resultSet.getString(5));

            preparedStatement.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(person.toString());


        return person;
    }

    public List<Person> getByName(String name){
        people.clear();
        try  {

            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM new_schema.persons WHERE NAME = ?");
            writeToList(name, people, preparedStatement);


        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Person p : people) {
            System.out.println("Получена запись: " + p);
        }


        return people;
    }

    private void writeToList(String name, List<Person> persons, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            persons.add(new Person(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getInt(4),
                    resultSet.getString(5)
            ));
        }
        preparedStatement.close();

    }

    public List<Person> getByLastName(String lname){
        people.clear();
        try  {

            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM new_schema.persons WHERE LNAME = ?");
            writeToList(lname, people, preparedStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Person p : people) {
            System.out.println("Получена запись: " + p);
        }
        System.out.println(people);
        return people;
    }

    public List<Person> getByAge(int age){
        people.clear();
        try  {

            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM new_schema.persons WHERE AGE = ?");
            preparedStatement.setInt(1, age);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                people.add(new Person(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getString(5)
                ));
            }
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Person p : people) {
            System.out.println("Получена запись: " + p);
        }

        return people;
    }

    public List<Person> getByCity(String city){
        people.clear();
        try  {
            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM new_schema.persons WHERE CITY = ?");
            writeToList(city, people, preparedStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Person p : people) {
            System.out.println("Получена запись: " + p);
        }

        return people;
    }
}

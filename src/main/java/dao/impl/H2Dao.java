package dao.impl;

import utils.H2Util;

import java.sql.Connection;
import java.sql.Statement;

public class H2Dao extends SqlDao {

    public void h2CreateTable(){
        try{
            Connection conn = getDBConnection();
            Statement stmt = conn.createStatement();
            String schema ="CREATE SCHEMA new_schema";
            stmt.executeUpdate(schema);
            String sql = "CREATE TABLE new_schema.persons(" +
                    "id bigint auto_increment PRIMARY KEY," +
                    " NAME VARCHAR(255)," +
                    " LNAME VARCHAR(255)," +
                    " AGE INT," +
                    " CITY VARCHAR(255));";
            stmt.executeUpdate(sql);
        } catch(Exception e){
            //    e.printStackTrace();
        }

    }

    public Connection getDBConnection() {
        return H2Util.getDBConnection();
    }

}

package dao;

import model.Person;


import java.util.List;

public interface PersonsDao {

    void create(Person p);

    List read();

    void update(Person p);

    void delete(int id);

}
